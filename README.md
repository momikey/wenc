# Wenc

Wenc is a strongly-typed binary network protocol for client-server IPC and RPC, based on CBOR.

This package defines Python types for the protocol, as well as encoding and decoding functions.

# License

Wenc is open source software under the [MIT license](LICENSE).
