# CLEF - Common Local Exchange Format

Clef ("Common Local Exchange Format") is a specification of a data format and protocol for communication between applications on the same machine or network. The goal is to provide a means for native applications to function in a client-server manner similar to that provided by in-browser apps and cloud services.

# Getting Started

This is a brief overview and introduction to the various parts of Clef.

## Connection

Clef is intended to work over local connections, but it does not require any specific kind of connection. It is expected that most Clef usage will be network or Unix domain sockets, usually over TCP. However, any kind of negotiated, bidirectional connection mechanism may be used instead.

## Data Format

Clef's binary data format is based on [CBOR](https://cbor.io), a standardized and extensible format defined in RFC 8949. In technical terms, a Clef data packet is a single CBOR object.

The core of the format is a request and response model. Clients send encoded requests to a server, which returns various encoded responses.

Clef data packets consist of three parts: a header, an options section, and a payload. Each will be described below.

### Header

Each Clef request or response is prefixed by a simple header structure. This structure is designed to be compact enough that only 16 bytes are needed to determine both the intent of the data packet and its size.

* Magic number - 4 bytes : defined as 0xda704177
* Clef command - 1 byte : in the range 0x60...0x6f
* Protocol marker - 1 byte in the range 0x84..0x97
* CBOR-encoded content length - 5 bytes
* Payload hash - 5 bytes

The magic number identifies the data packet as being in the Clef format. It is, in fact, the first 4 bytes of a CBOR tag, of which the command is the fifth and final byte.

The protocol marker is currently defined as the fixed byte 0x84. This is the start of a CBOR array of length 4 (major type 4, additional information 4), corresponding to the sequence of content length, payload hash, options section, and payload.

The content length field is always encoded in the format defined in CBOR as major type 0, additional information 26, even if the length of the data packet is small enough that it would fit in fewer than 4 bytes. The content length *does not* include the 16 bytes of the header.

The payload hash is a CBOR-encoded 32-bit integer. Its value must be the 4-byte BLAKE2s hash of the options and payload sections concatenated into a single string of bytes. Clients and servers must check that the hash given in a packet matches that of the packet's payload. Note that the payload hash is explicitly *not* intended for authentication, authorization, or any uses in which a cryptographically secure hash with large digest size is needed. Its purpose is as a first line of defense against network errors, not malicious actors. The standard options 0x0d (Signature) and 0x0e (Signing Algorithm), which can be added to any Clef packet, are more appropriate for the latter.

As CBOR is unfortunately defined to be big-endian, this means that numbers are in the wrong byte order, so implementations must be aware of the need to convert.

### Operations

Clef uses only the lowest 4 bits of the header's command field, fixing the upper four bits to 0b0110. Thus, there are 16 possible opcodes.

* 0x0: Request
* 0x1: Response
* 0x2: Discovery Query
* 0x3: Discovery Response
* 0x4: TLS negotiation
* 0x5: TLS response
* 0x6: Authenticate
* 0x7: Authentication Response
* 0x8: Batch
* 0x9: Error Response
* 0xa: reserved
* 0xb: reserved
* 0xc: Continue
* 0xd: reserved
* 0xe: Acknowledge
* 0xf: Disconnect

#### 0x0: Request

The Request opcode is the primary means of communication from the client to the server. Clients issue Request packets to a listening server, which will respond based on the options and payload and the application's needs.

Servers will not use Request to communicate back to a client. Instead, upon receiving and processing a Request, they will send a Response, Error Response, Acknowledge, or Disconnect packet as appropriate.

Client sent: The server should respond in a manner appropriate for the request's options and payload. For the most part, the response format is application-defined.
Server sent: The server is not permitted to send a packet using this opcode. Clients may ignore or discard any Request packets they receive.
Options:
    * 0x20 - A string containing the name of the service. Servers may implement a default service that does not require this option to be present. Otherwise, the absence of this option is an error.
Payload: The payload is in the format of the request payload, shown below.

#### 0x1: Response

The Response opcode is the converse of Request, and is the primary means for a server to communicate to a client. Servers issue Response packets to clients in response to those clients' requests.

Responses must be sent only when replying to a client's request. In addition, a Response packet is not semantically tagged as an error, so Error Response is more appropriate for signaling errors or exceptional conditions.

Client sent: The client is not permitted to send Response packets. If a server receives a Response packet, it may reply with an Error Response with error code 0x7f (Invalid Operation). Servers are also permitted to initiate a disconnection using the Disconnect opcode, though this should only be used in cases of repeated or malicious Response sending.
Server sent: Application-defined. In most cases, the Response payload contains data that the client has requested, so the client will act upon it appropriately.
Options: None defined.
Payload: Application-defined.

#### 0x2: Discovery Query

The Discovery Query opcode is used by the client to ask the server about services it provides. Services are named by strings.

Client sent: The server should respond with an appropiate Discovery Respone packet. If the client requests only services that the server does not provide, the server may respond with an Error Response with error code 0x37 (No Service Match) instead.
Server sent: The server is not permitted to send this opcode. A client may ignore any packets using it.
Options:
    * 0x40 - A single string or array of strings representing one or more resource names which the client wishes to discover. Alternatively, the single value `null` may be used. In this case, the server must respond with a list of all services it provides.
    * 0x41 - A boolean value indicating whether the client is requesting verbose information regarding the service or services. If `true`, then the server should respond with greater detail regarding the services, which may include parameters, payload format, documentation, and so on.
    * 0x42 - A boolean value indicating whether the server should return information for all versions of a service, rather than just the latest version.
Payload: No payload is defined.

#### 0x3: Discovery Response

The Discovery Response opcode is used by a server to return data to the client about resources it provides and requests that it accepts. This is always in response to a Discovery Query packet.

Client sent: The client is not permitted to send this opcode. Servers may choose to ignore it, or to respond with an Error Response packet using error code 0x7f (Invalid Operation).
Server sent: The client may use the data in the packet's payload as a guide to send further requests to the server.
Options: None defined.
Payload: The payload for a single discovery response is shown below. If the Service Description option was set to `false` in the Discovery Query packet, only those fields marked as "basic" are included.
    * If the client requested a single service in its Discovery Query, then the payload for this packet is a single object as defined below.
    * If the client requested multiple services in an array, then the payload will be a map of service names (consisting only of the set requested by the client) to discovery response objects.
    * If the client's request used a `null` option to retrieve the entire set of resources provided by the server, then the payload is a map of service names (consisting of all those provided) to discovery response objects.
    * If the client set the Version Discover option to `true`, then the payload for a single service query is an array of discovery response objects, one for each supported version. For discovery queries requesting multiple or all services, servers may choose to return either a map of service names to arrays of response objects, or an Error Response packet with error code 0x30 (Refused).

#### 0x4: TLS Negotiation

The TLS Negotiation opcode is used by the client to inform the server that it wishes to begin a secured connection. It has no equivalent in HTTP, but is similar to the STARTTLS command in other protocols.

Client sent: The server must respond with a TLS Response packet if TLS is available for this connection, or an Error Response packet with the error code set to 0x3f (Not Implemented) if the connection cannot be secured using opportunistic TLS.
Server sent: The server is not permitted to send this opcode. If a client receives it, that client is permitted to ignore the packet entirely.
Options: None defined.
Payload: No payload is necessary.

#### 0x5: TLS Response

The TLS Response opcode is used by the server to inform a client that it is ready to begin a TLS handshake. This is the server counterpart to TLS Negotiation.

Servers must allow clients to connect using at least the current version of the TLS spec and, for compatibility, the version immediately prior. At the present time, these are versions 1.3 (current) and 1.2 (prior).

Client sent: The client is not permitted to send this opcode. If a server receives it, that server may respond with an Error Response or a Disconnect packet, or it may ignore the message.
Server sent: The client is now allowed to begin a TLS handshake. How this happens is implementation-defined, but the result of the handshake must be that all communication between this client and server for the remainder of the session must be over a secure connection.
Options: None defined.
Payload: No payload is necessary.

#### 0x6: Authenticate

The Authenticate opcode is used by a client to send authentication information to the server to be allowed access to privileged or restricted resources.

Authentication may be done using a username/password combination, some means of two-factor authentication, or any other application-defined method. Servers, if they implement any resources requiring authentication, must implement at least support for either username/password or an OS-provided authentication mechanism (e.g., PAM).

Client sent: The server must check the credentials provided by the client and respond with either an Authentication Response containing an authorization token to be used in future requests or an Error Response that uses error code 0x39 (Incorrect Credentials). Servers may include additional metadata such as tracking IDs.
Server sent: The server is not permitted to send this opcode, and clients may ignore packets containing it.
Options:
    * 0x50 - A text string containing a username.
    * 0x51 - A text string containing a password. Servers may reject packets sent with this option that use insecure connections.
    * 0x52 - A string of text or bytes containing an authentication token that is neither a username nor a password. This is intended for two-factor authentication codes, etc.
Payload: No payload is necessary.

#### 0x7: Authentication Response

The Authentication Response opcode is used by a server to respond to a successful Authentication packet sent by a client. If the client's authentication credentials are correct, then the server sends this opcode in response, along with an identifying code or token that may be used in future requests to grant authorization.

The format of the authorization token is not specified. It may be a JSON Web Token, a CBOR Web Token, or any other format, though the first two options are preferred for interoperability.

Client sent: The client is not permitted to send this opcode. Servers receiving it should respond with an Error Response packet using error code 0x7f (Invalid Operation). Servers may instead send a Disconnect packet and terminate the connection, if the client's request is deemed malicious.
Server sent: The client should store the authorization token contained in the payload so that it can be used in future requests.
Options: None defined.
Payload: A single string of bytes or text that is the authorization token. The format is application-defined, and should be considered by clients to be opaque.

#### 0x8: Batch

The Batch opcode allows a client to issue multiple parallel requests in a single transmission, and a server to respond to all of those requests at once.

Options that are set on the Batch packet are considered the default state for each request within the batch. Each request may add its own options, as well. If an option is repeated in both the Batch and the inner request, the inner request's version takes precedence.

Client sent: Only Request, Discovery Query, Continue, and Acknowledge packets are allowed to be contained within the Batch. The server must act upon each of thesusual, with the exception that it must not discard any packets. Instead, a single null value (byte 0xf6) must be sent as the corresponding item in the response.
Server sent: Only Response, Discovery Response, Error Response, Continue, and Acknowledge packets are allowed to be contained within the Batch. Each item in the payload is the server's response to the corresponding index of the client's Batch packet.
Options: None defined. All options set on the Batch are handled as if they were set on each individual item within the batch.
Payload: A batch payload consisting only of an array of items, each item being a Clef packet.

#### 0x9: Error Response

The Error Response opcode is used by the server to signal errors. It is a catch-all for any kind of error or exceptional condition, and it roughly translates to HTTP status codes 4xx or 5xx.

Client sent: The client is not permitted to send this opcode. If a server receives an Error Response packet, it must send its own Error Response in return, with the payload's error code (see below) set to the value 0x7f (Invalid Operation).
Server sent: The client must handle the error in whatever way is appropriate for the application and the situation.
Options: None defined.
Payload: An error payload (see below).

#### 0xc: Continue

The Continue opcode is used by the client to request that the server continue transmitting data packets in the same manner as a previous request/response cycle. It is intended for streaming applications or repetitive calls such as pings or keep-alive operations.

If the continuuation marker is set to any nonzero value, then the server may assume that the client wishes to continue the operation that resulted in a response tagged with this marker. If the continuation marker is not set, the server must assume that the client's Continue request is in reference to the last response sent.

What "Continue" means in the context of an application's requests is application-defined, but it should be a sensible operation. For example, an API for sequence data such as a database cursor may let clients iterate over the sequence by using Continue.

Client sent: The server should return a response packet of the same type as previously sent, with application-defined semantics. If the Continue operation does not make sense in the current context, then the server may instead response with an Error Response message.
Server sent: The server is not permitted to send this opcode. If a client receives a Continue data packet, it is free to ignore the entire message.
Options:
    * 0x17: Continuation Marker (integer or string of bytes, no default)
Payload: No payload is necessary. Applications may define payloads that are meaningful in this context.

#### 0xe: Acknowledge

The Acknowledge opcode is a catch-all command for general acknowledgment that a request was received or an action was taken. It can be considered roughly analogous to the HTTP 204 status code.

Client sent: No action needed. The server may perform any housekeeping tasks associated with the client, but must not assume that any state has changed except that resulting from the acknowledged command.
Server sent: No action needed. The client may assume that the server is ready to receive a new request.
Options: None defined.
Payload: No payload is necessary.

#### 0xf: Disconnect

The Disonnect opcode is simply a request for a graceful shutdown of the connection between client and server.

Client sent: The server should save any state associated with the client, send an Acknowledge command, and close the socket (or other connection).
Server sent: The client should immediately close its connection to the server in the safest way possible.
Options: None defined.
Payload: No payload is necessary, but applications may allow servers to interpret a custom payload as instructions for the shutdown procedure or hints regarding the saving of state.

### Options section

The options section is a CBOR-encoded map of positive integers of up to 4 bytes in length or variable-length strings to data values (integers, strings, arrays, maps, booleans, or the special value `null`). Options are for changing the way a request is handled or a response should be interpreted. The absence of an option means that it should be assumed as its default value. Even if no options are specified, this section is required to be present. An options section with no options used must be encoded as an empty map, represented by the single byte 0xa0.

All integer key values are reserved, with those in the range 0x00...0x17 intended for protocol-level options and values starting at 0x18 intended for standardized options. All string key values are application-defined, with the exception that applications are not permitted to define string keys whose values are simple conversions of integers to strings, such as "42" or "88".

All defined integer keys have associated default values. Clients and servers must assume these defaults if the respective option is not present in this section.

#### Protocol options

* 0x01 - Protocol Version (positive integer, default 1) : The Clef protocol version. This may be used in the future to allow for expanding the Clef specification while retaining backward compatibility.
* 0x03 - Session Identifier (string of bytes, no default): A session identifier. The server may set this on a Response packet to indicate to the client that it is performing state tracking for the duration of a session. If a client receives a Response with a session identifier set, it should set this option to that value for other Request packets that are logically connected to the one that triggered this Response.
* 0x05 - Authorization (string of bytes or characters, or `null`, default `null`) : A token, provided by the server, that allows a client to make authorized requests. This can be in any format, but a JSON Web Token or a CBOR Web Token are preferred.
* 0x08 - Timestamp (integer, string of characters, or `null`, default `null`) : A timestamp for the request or response, recorded as one of the following: an integer number of seconds before or after the UNIX epoch; a date-time string in the format described by RFC 4287; or a CBOR tagged data item using tags 0 or 1. A null value indicates that this field has no value.
* 0x09 - Expires (integer, string of characters, or `null`, default `null`) : A timestamp indicating an expiration time for the data given in the request or response. This option should only be specified if the 0x08 (Timestamp) option is also used, and it should be in the same format as that option.
* 0x0d - Signature (string of bytes or `null`, default `null`): A cryptographic signature of the data in the packet, excluding the header and this option, but including all other options and the payload. This may be either a simple hash (using a cryptographically secure algorithm) or a signature dervied from an asymmetric algorithm such as HMAC. If null, then no signature is present.
* 0x0e - Signing Algorithm (string or `null`, default `null`): The algorithm used to create the value in the Signature option. This is a case-insensitive string that should name the algorithm in a distinctive manner, such as "HMAC" or "sha256".
* 0x10 - Ping (boolean, no default): If this is set to `true`, the packet is a "ping" packet intended only to test that a server exists. Servers should respond with either an Acknowledge response or a Response packet whose payload is an object with informative content on how to access the server's resources. This object need not be in the same format as a Discovery Response.
* 0x17 - Continuation Marker (integer or string of bytes, no default): If this is set to any value by a server, it indicates that the Response or Discovery Response is incomplete. The client can then make a Continue request using this marker, and the server must respond with a Response or Discovery Response (whichever caused this option to be sent) that continues the data that was interrupted. After a continuation marker is used once, it is no longer valid, and another continuation marker must be sent to indicate further incomplete data.

#### Standardized options

* 0x19 - Last Modified (date/time text string with CBOR tag type 0, no default): Used with a Response to indicate the date of the last modification for the requested resource. This option is not required; if it is used, it must be accurate enough that a client can rely on its value.
* 0x1e - Range (array of 2 unsigned integers, no default): Used with a Request to indicate that the client only wishes a subset of the response data. Used with a Response to indicate that only the data within a range was returned. The "range" is defined contextually, depending on the requested resource. For a resource that normally returns an array or list of data, Range with the values `[start, end]` indicates that the response should only return items of the list starting at index `start` and ending at `end`, including both endpoints. For resource that return binary data, Range indicates the start and end byte offsets to return. For other resources, this option is application-defined.
* 0x20 - Service Name (string, no default): Used with a Request to identify the service for which the request is being made. If not set, a server may choose an application-defined default or respond with an Error Response packet containing error code 0x37 (No Service Match).
* 0x21 - Service Version (integer or byte string, no default): Used with a Request to specify a specific version of a service, such as an API version.
* 0x3e - Target (text string, no default): Used with a Request to indicate the target of a request. This is a kind of "indirect object" whose semantics are application-defined.
* 0x3f - Instance Identifier (integer, byte string, or text string, no default): Used with a Request to request a specific instance of a resource, such as a data item with a given database ID. Used with an Acknowledge or Response to indicate that a resource with the specific identifer has been modified.
* 0x40 - Service Discovery (string, array of strings, or `null`, default `null`): Used with a Discovery Query to specify the service(s) a client wishes to discover. A single string specifies one service. An array of strings indicates that the client wishes to query the server for all services in the array. A null value instructs the server to provide a list of discoverable services.
* 0x41 - Service Description (boolean, default `false`): Used with a Discovery Query to specify the requested verbosity of the response. If `true`, the server should include in its response more detailed information about the service, including options accepted and payload expected.
* 0x42 - Version Discover (boolean, default `false`): User with a Discovery Query to specify that all versions of a service should be returned, instead of only the latest.
* 0x50 - Username (string of characters, default `null`): Used with an Authenticate packet to indicate the client's username.
* 0x51 - Password (string of characters, default `null`): Used with an Authenticate packet to indicate the client's password: Server implementatios should strongly consider only allowing this option on secured connections.
* 0x52 - Authentication Token (string of characters, default `null`): An application-defined token used with an Authenticate packet to indicate the client's identity. This may be, for example, a token from a two-factor authentication device, am HTTP login cookie, etc.

#### Private Use options

Certain blocks of integers are allocated as Private Use options. These will never be assigned specific meanings, and applications which have requirements that are incompatible with string keys (e.g., applications running on networked devices with very low memory) are allowed to use them however they wish.

* 0xc0-0xff: Low Private Use area (64 values)
* 0xc000-0xffff: Mid Private Use area (16,384 values)
* 0xc000_0000-0xffff_ffff: High Private Use area (1,073,741,824 values)

#### Reserved options

All integers not defined above as Protocol, Standard, or Private Use options should be considered reserved for future use, but some blocks are reserved specifically for various reasons.

* 0x80-0x9f: Reserved for a future "Clef Web" specification
* 0x8000-0x8fff: Reserved for a future "Clef Web" specification

### Payload

The format of a Clef data packet's payload must be one of the following:

* A single scalar value: an integer, floating-point number, boolean value, or the special value `null`
* A single UTF-8 text string
* A single binary data block (encoded in CBOR as a byte string with major type 2)
* An array of values
* A map of values

The payload is required. If an application does not need a payload for a specific command, then the special value `null` (CBOR major type 7, additional information 22) may be passed as filler.

CBOR sequences of more than one item are not allowed as a Clef packet's payload.

Unless otherwise specified by an opcode's specification, the payload is application-defined.

#### Request

The Request opcode defines its own payload, which is encoded as an array of two items:

* A map, with each key being the name of a formal parameter defined by the service, and the corresponding value being the submitted value of that parameter.
* An array of unnamed parameters.

If either of these is not needed by the requested service, the client must still include it as an empty map or array.

#### Discovery response

The Discovery Response opcode defines its own payload, which can be either a single object or a map of text strings to objects. The object has this format, which is encoded as a CBOR map:

* name (*extended*, text string, no default) - The name of the service, which is the way it is addressed in Request packets.
* description (*extended*, text string, no default) - A plain text description of the service, which may be displayed to users.
* parameters (*basic*, array, no default) - A list of parameters this resource accepts or requires. Each entry in the array is an object in the following format, encoded as a CBOR map:
    * name (text string, no default) - The formal name of the parameter. This may be displayed to the user. The name is optional; if not given, then this is a positional parameter.
    * type (integer or text string, no default) - The type of this parameter. This can be specified as an application-defined type using a string, or as a "common" type using one of the integer type codes below.
    * req (boolean, default `false`) - Whether this parameter is required.
    * def (variant type, default `null`) - The default value for this parameter, if any. Note that the specification does not distinguish between a parameter with a default value of `null` and one with no default.
* auth (*basic*, boolean, default `false`) - Whether this resource requires authorization to use.
* version (*extended*, integer or byte string, no default) - The version of this service. Servers may allow clients to access older versions of a resource or service for compatibility by sending requests with the Request Version option set.
* latest (*extended*, boolean, no default) - Whether this is the latest available version of the service. This parameter is only allowed if the version parameter is also set. If true, then this is the latest version of the given service.

Applications may define additional fields for this object.

##### Type codes

* 0x00 - 8-bit unsigned integer
* 0x01 - 16-bit unsigned integer
* 0x02 - 32-bit unsigned integer
* 0x03 - 64-bit unsigned integer
* 0x04 - 8-bit signed integer
* 0x05 - 16-bit signed integer
* 0x06 - 32-bit signed integer
* 0x07 - 64-bit signed integer
* 0x08 - 16-bit floating-point number
* 0x09 - 32-bit floating-point number
* 0x0a - 64-bit floating-point number
* 0x0c - boolean
* 0x10 - text string
* 0x11 - pointer
* 0x12 - array
* 0x13 - map
* 0x14 - binary data (string of bytes)
* 0x15 - datetime string (text string with CBOR tag type 0)
* 0x16 - timestamp (floating-point number with CBOR tag type 1)

#### Error Payload

The Error Response opcode defines its own payload. This is an array of values as follows:

* Error code (unsigned integer in the range 0x20-0x7f; major type 0, additional information 24)
* Error description (text string; major type 3, additional information varies based on string length)
* Additional information (map of application-defined values; major type 5, additional information varies)

The error code can be any integer in the range 0x20-0x7f, with values 0x40-0x5a (Latin uppercase letters and the "@" sign in ASCII) reserved for private application use. All others are either listed below or should be considered reserved.

* 0x22 (Validation Failure): The client's request is in the correct format, but failed a validation constraint.
* 0x24 (Payment Required): The server requires a payment to proceed. This may be used for usage-based billing of networked resources, for example.
* 0x25 (Hash Failure): The packet header's payload hash field does not match the hash of the payload.
* 0x2a (Instance Unavailable): The client requested a specific instance of a resource using the Instance Identifier option, but the server cannot provide that instance.
* 0x2e (Expired): The client's request has expired or is no longer valid.
* 0x2f (Bad Request): The client's request was not in the proper format.
* 0x30 (Refused): The server is refusing to process the client's request. The error description and/or additional information must explain the reason.
* 0x34 (Version Unavailable): The client has made a request for a specific version of a service that the server provides, but this version is not supported by the server.
* 0x37 (No Service Match): The client has made a request for a service that the server does not provide.
* 0x38 (Unauthorized): The server requires authorization for this requeest.
* 0x39 (Incorrect Credentials): The client provided incorrect credentials for a request for authorization.
* 0x3a (Forbidden): The server is refusing to act on the client's request because the client is not allowed to access a resource, even if it is authorized.
* 0x3b (Authorization Expired): The client provided a correct credential, but it has expired and is thus no longer valid.
* 0x3d (Conflict): The client has requested a change in data or state that conflicts with current or pending state.
* 0x3f (Not Implemented): The client has made a request for which the server has not implemented support.
* 0x5f (Encryption Required): The client attempted to make a request over an unencrypted connection, but the server requires TLS.
* 0x70 (Server Error): The server is unable to process the client's request because of an internal error.
* 0x71 (Temporarily Unavailable): The resource requested is temporarily unavailable, or the server is incapable of providing it in response
* 0x72 (Rate Limit Exceeded): The server has refused the request because the client has made too many requests in a given time.
* 0x73 (Insufficient Resources): The server is unable to process the client's request because of a lack of resources such as disk space, bandwidth, or memory.
* 0x7c (Unsupported Protocol): The client sent a request using an unsupported version of the Clef protocol.
* 0x7f (Invalid Operation): The client attempted to send a server-specific opcode such as 0x9 Error Response.

The error description is freeform text, and clients should not rely on its value. Servers should provide error descriptions suitable for display in debug logs.

The additional information map is required, but may be an empty map (byte 0xa0).

Values in the range 0x60-0x6f are reserved for a future "Clef Web" specification.

## Network Protocols

It is expected that most Clef data packets will be sent using TCP or UDP. If UDP is used, then implementations must handle acknowledgment of received packets. Implementations may support other network protocols as they wish, but they are responsible for usage.

## Service Discovery

Discovery of services and resources provided by a server is handled through Discovery Query and Discovery Response packets. Clients send a Discovery Query to the server to learn about how to request services, and servers use Discovery Response to describe these services in a programmatic fashion.

Discovering available servers is more difficult. Small systems, where the number of resources is small and servers are not expected to be interchanged, may hardcode names and port numbers (assuming the client and server communicate over a protocol such as TCP).

For larger systems, or instances where a client might connect to many different servers at once, a metaserver may be used. This metaserver should also be a Clef server. One possibility is that this server responds to a Request using the Ping option by returning a list of servers that have registered with it, along with the necessary infomration to connect to them.

Clef reserves the port number **7476** as its default port over TCP and UDP. If a system uses a metaserver, this metaserver should listen on the default port.

## Security

Clef provides for opportunistic TLS. Implementations are not required to support this, but it is strongle recommended if any of the following are true:
* The server will be used over an insecure or pulbic network.
* The server has access to sensitive data or personally identifying information.
* Arbitrary, and possibly malicious, clients will connect to the server.

Instead of opportunistic TLS, an implementation may require all connections to be secure at all times. This is allowed by the specification. In this case, the TLS Negotiation and TLS Response opcodes are unused, and a server may disconnect without warning if it determines that the connection's security is compromised in any way.
