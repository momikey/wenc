# Introduction

Wenc is an extension of the Clef protocol to create a stateless, client-server protocol for web-like applications.

# Goal

Wenc is not intended to replace HTTP for general use. Instead, it aims to be a standard for native applications (so-called "thick clients") to access data and server-side functionality on remote machines without the necessity of embedding an entire web browser or rendering engine.

The way Wenc does this is by defining a common binary packet format - based on the extensible Clef protocol and the CBOR standard - and a number of actions which can be performed on data.

# General considerations

Wenc is action-oriented rather than document-oriented. A single action can perform the usual CRUD operations of a REST API, as well as metadata-related and locking actions.

The model is client-server, and client-initiated. Data returned from the server is in a resource-specific format: it may be, for example, a text document such as HTML or Markdown; binary data representing an image file; or a set of data elements whose interpretation is application-specific.

Errors, authentication, service discovery, and TLS negotiation all use the standard Clef mechanisms. No new opcodes are added, as the current Clef functionality is adequate for Wenc.

# Clef extensions

Wenc adds new standardized options using integer values in the range 0x80-0x9f, which the Clef standard has reserved for the explicit purpose of a Clef Web specification. In addition, Wenc expands the set of error response codes using the range 0x60-0x6f, also reserved by the Clef standard for the same purpose.

The new options are described below, but can be summarized here:

* Request Action (0x80) is the type of action the client is requesting to take. It is broadly similar to the HTTP method (e.g., GET, POST), though with differences that will be explained later. This option is required in all Wenc requests, so a server may use it as a way to distinguish Wenc requests from other Clef communication.

* Request Type (0x82) is an option that specifies the MIME type or types of the client request's payload. Clef requires that the payload be a CBOR type, but this option allows further specification for text string and byte string types. Arrays and maps are allowed for "multipart" requests.

* Response Type (0x83) serves two purposes. For servers, it specifies the MIME type or types of the response payload. For clients, it names the expected response type as a hint to the server.

* Referrer (0x84) is a non-mandatory option that allows clients to indicate that they have been redirected from another server. Unlike the corresponding HTTP header, it is spelled correctly.

* Request Agent (0x85) and Response Agent (0x87) are simple text options that allow clients and servers to identify themselves.

* Compression (0x8c) is a dual-purpose option. For servers, it gives the name of the compression algorithm used on the payload, if any. For clients, it informs the server of compression algorithms that are supported.

* Valid Until (0x8e) is a non-mandatory option that allows servers to send an expiration timestamp to a client.

* Path (0x8f) is a non-mandatory option that allows servers to inform clients of any additional steps that were taken in processing a request, such as proxying.

The addtional error codes defined by Wenc fill some gaps in the standard set provided by Clef, specifically for the purpose of data transfer in web applications. They are described later, but summarized here:

* Unsupported Action (0x60) is raised when the server does not support the client's specified Request Action for a resource.
* Moved (0x62) and Moved Temporarily (0x64) inform the client that a requested resource is not available at the requested location, but has been moved. The difference between the two is that Moved implies the resource's permanent, canonical location has changed.
* Censored Content (0x66) allows servers to comply with legal requests while still providing client users with enough information to exercise their natural rights. Unlike most "takedown" schemes, Wenc requires servers to provide both legal justification and a dispute method in the event of any data removal.
* Unsupported Type (0x68) tells a client that the Request Type is not supported by the server.
* Locked (0x69) is raised when a client attempts to write to a locked resource, or to read from a resrouce that has an exclusive lock.
* Timeout (0x6d) allows a server to warn a client that it has taken too long for an expected request.
* Too Large (0x6e) allows a server to indicate that it has a payload size limit, and to reject payloads larger than that limit.

# Standardized resource formats

## Addressing formats

Wenc resources may be addressed in one of two ways. The native addressing format consists of four parts: scheme, host, path, and version, with the host and version portions optional. Formally:

`address = "wenc:" [ hostname [ ":" port ] ] ":" path [ "@" version ]`

The scheme is always `wenc`, and is suffixed with a colon. As Wenc, like Clef, uses opportunistic TLS, there is no need for a separate "secure Wenc" scheme.

The host can be in any format allowed by the URI host definition in RFC 3986, including a domain name, IPv4 or IPv6 address, or local network hostname. The host portion of the address is suffixed with a colon. The entire portion, except for the final colon, may be omitted; in this case, the local machine is assumed.

The port may be any TCP port. If not present, it is assumed to be the default Clef port `7476`.

The path is a dot-separated list of strings indicating a hierarchical path to a resource. It is a required component of the address. If the path begins with a period character (`.`), then it is treated as relative to a certain, context-specific path, which the server may treat as being prefixed to this portion of the address.

The version identifier is prefixed with `@`, and is a simple integer or a hex string. It is an optional component intended to indicate to a server which version of a resource is being requested. If given as a hex string, the server must convert it to a binary value before using it to determine what response to send to the client. If this portion of the address is omitted, the server should assume the latest version of the requested resource is intended.

### Compatibility addresses

For compatibility purposes, Wenc also defines a URI format:

`wenc-uri = "wenc://" [ hostname [ ":" port ] ] "/" [ path ] "/" [ version ]`

In this form, the host and version are still optional. Relative paths are not allowed.

## Payload formats

# New options

## Request Action

### Action types

#### Get

The Get action type (Request Action code 0x00) is used by a client to request a single resource from a server. The resource may be a document, binary data, or any other type, including aggregate types such as arrays, lists, associative arrays, or objects.

The server should respond with a Response containing the requested resource if it is available, or an Error Response with a suitable error code otherwise. The format of the response payload is application-specific, but servers should set an appropriate Response Type when returning data.

A Get action is idempotent and atomic, and must not change the visible state of the resource. Servers may update internal, private state of the resource while handling a Get action.

#### List

The List action type (Request Action code 0x01) is used by the client to request a list of related resources from a server.

Semantically, the difference between a List action and a Get action that returns an array is that the List action implies that the data returned is a collection. Servers may use the Range and Continuation Marker options to permit paged or "cursor-like" retrieval.

The server should respond to a List action with an array of data if that data is available, or an Error Response otherwise.

If the Range option is set, but only the start point of the range is among the available data items for a resourcce, the server must provide as many items as possible that fall within the requested range.

A List action is idempotent and atomic, and must not change the visible state of any object requested.

#### Create

The Create action (Request Action code 0x02) is used by the client to create a new instance of a resource on the server.

The server should respond to a Create action with an Acknowledge or Response packet if the creation is successful, or an appropriate Error Response otherwise. If the action created a new instance of the resource, such as an entry in a database, the server may use the Instance Identifier option to notify the client of this instance's ID.

A Create action is atomic but not idempotent. It may change any visible or private state of the resource.

#### Update

The Update action (Request Action code 0x03) is used by the client to update an existing resource, or an instance of an existing resource, on the server.

The server should respond with an Acknowledge or Response packet if the update was successful, or an appropriate Error Response otherwise. The format of the response payload is application-defined.

An Update action is atomic but not idempotent. It may change any private state of the resource, and any visible state that the client requested to be updated. Servers must not change any visible state that was not changed in the client's request.

#### Delete

The Delete action (Request Action code 0x04) is used by the client to delete a resource or instance of a resource on a server.

The server should respond with an Acknowledge if the deletion was successful, or an appropriate Error Response otherwise. Servers should limit Delete actions to authorized users only.

A Delete action is atomic but not idempotent. It may change the private state of the resource. After a successful Delete, a Get action for the same resource or instance of a resource must result in an Error Response with a No Service Match (0x37) or Censored Content (0x66) error code, whichever is more appropriate, until the resource is recreated through a Create action or direct server operation.

#### Copy and Move

The Copy action (Request Action code 0x05) and Move action (code 0x06) are used by the client to copy or move data from one resource to another on a server. The Target option, required for both action types, defines the destination of the copy.

The server should respond with an Acknowledge or Response if successful, or an Error Response otherwise. The semantics of the response are the same as for the Create action. If a resource already exists at the destination, the server should respond with an Error Response using the Conflict (0x3d) error code.

If the client sets the Instance Identifier option, servers must only copy or move a single instance of the resource, and may assign the result a new Instance Identifier. In this case, an Error Response with the Conflict error code should be returned only if a new instance of the target resource would conflict with an existing instance of the same resource, e.g., a duplicated primary key in a database.

A Copy action is atomic but not idempotent. It should not change the visible state of the original resource, and the new resource should have the same visible state, with the exception that unique identifers such as database primary keys, UUIDs, and timestamps may be changed.

A Move action is atomic but not idempotent. The moved resource should have the same visible state, with the exception that unique identifers, defined in the same manner as for the Copy action above, may be changed. Future attempts to access the resource at its original location should result in an Error Response with a Moved or Moved Temporarily (0x62 or 0x63) error code, whichever is more approopriate.

#### Options

The Options action (Request Action code 0x08) is used by the client to retrieve the options that would be returned in a request for a resource, but without the payload of that resource.

Servers must respond exactly as if they had received a Get request for the requested resource, except that they must not return the data for that resource in the response payload. If any options are filled dynamically based on ephemeral data from the resource, only these options may be set to null, default, or placeholder values.

An Options action is atomic and idempotent. It has the same semantics as the Get action for the same resource or instance

#### Lock and Exclusive Lock

The Lock (Request Action code 0x0c) and Exclusive Lock (code 0x0e) actions are used by the client to indicate that it wishes to acquire a lock on a given resource. Semantically, Lock indicates a shared lock that allows other clients to read the resource concurrently, but not write it, while Exclusive Lock prevents other clients from reading from or writing to the resource while the lock is active.

The server should respond with an Acknowledge if the resource was successfully locked, or an Error Response otherwise. The code of the Error Response should inndicate why the resource could not be locked.

Clients may set the Instance Identifier option. If this is set, the server must only lock the requested instance of the resource, if it exists.

A server may choose not to implement lockable resources at all. If this is the case, the server must respond to any and all Lock and Exclusive Lock requests with an Error Response using the Unsupported Action (0x60) error code.

A server which supports locking may limit the length of time a lock can be active, to prevent deadlocks. If so, the server must set the Valid Until option on its Acknowledge response to the Lock or Exclusive Lock request to the expiration time of the locking request.

Lock and Exclusive Lock are atomic and idempotent. Locking must not change any visible state of a resource or instance, but servers may alter private state. Locking a resource that is already locked from a previous use of the same type of action should have no visible effect, but a server may extend the expiration time of the resource's lock. A server receiving an Exclusive Lock for a resource that is currently Locked by the same client must "upgrade" the lock from shared to exclusive if the action is permitted.

#### Unlock

Unlock (Request Action code 0x0d) is used by the client to indicate that it wishes to remove a lock it has previously set on a resource using the Lock or Exclusive Lock actions.

The server should respond with an Acknowledge if the resource was successfully unlocked, or an appropriate Error Response otherwise.

Clients may set the Instance Identifier option. If this is set, the server must only unlock the requested instance of the resource, if it exists and is locked.

A server may choose not to implement lockable resources at all. If this is the case, the server must respond to any and all Unlock requests with an Error Response using the Unsupported Action (0x60) error code.

Unlock is atomic and idempotent. Unlocking a resource or instance must not change any other visible state of that resource or instance, but servers may alter private state. Unlocking a resource that is not locked has no effect. In this case, servers may choose to return an Error Response, but that response must use then the Conflict (0x3d) error code.

## Request Type

The payload of a Wenc request is always strongly-typed, and it can be any object which can be represented by a single CBOR object, including simple integers, text strings, binary data, lists, maps, and so on. For data encoded as strings or binary blobs, it may also be necessary to identify the MIME type of that data, for easier processing by external utilities. For example, a client making a request to upload an image may wish to identify the image file format.

The Request Type option (code 0x82) allows clients to specify the MIME type of a request's payload. This option may be a single string, an array of strings, or a map of strings to strings.

In all cases, a server must send an Error Response using the Unsupported Type error code if it cannot understand or handle the requested type or types.

### String option

The simplest form of Request Type is the string option. In this case, the entirety of the payload is treated as being of a single MIME type. By default (assuming no Request Type option is used), this type is `text/plain` for text strings, `application/octet-stream` for byte strings, and `application/cbor` for any other data type.

Clients may set this to any valid MIME type string to identify the content of the payload. This data must still be encoded as a CBOR object, but servers must interpret the decoded data as if it were the specified type.

### Array option

If and only if the payload is an array, clients may specify an array of Request Types. In this case, the specified type strings are in a one-to-one correspondence with the objects in the payload array. Thus, the first string in the Request Type array is the MIME type of the first object in the payload, and so on.

The special value `null` (CBOR byte 0xf6) may be used to inndicate that the MIME type of an object is the default type: `text/plain` for text strings, `application/octet-stream` for byte strings, or `application/cbor` for all other data objects.

### Map option

If and only if the payload is a map of strings to data objects, clients may specify a map of Request Types. The keys of this map are strings, and each should match a key in the payload map. The value of a key K in the Request Type map is the MIME type of the value of key K in the payload map.

If a key in the Request Type map is not present in the payload map, servers must ignore this key without returning an Error Response. If a key in the payload map is not present in the Request Type map, its MIME type should be the default, as indicated above.

## Response Type

The Response Type option (code 0x83) indicates the type or types of the response payload when sent by a server. In this respect, its format is identical to that of the Request Type option above. String, array, and map options are all supported, with the same restrictions and defaults as for the Request Type.

In addition, a client may send a request with the Response Type option to indicate that it will accept and prefer a response of the specified type. In this case, the option's value must be either a string or an array of strings. If it is a string, then this is the accepted MIME type. If it is an array, it is a set of accepted types. Clients may use wildcard MIME type specifications such as `image/*` in either the string or the array versions of this option.

## Referrer

The Referrer option (code 0x84) is a metadata option. Clients may add the Referrer option to indicate that they have been redirected from another server. The format of the option's value is application-defined, but it should include, at a minimum, the host name of the referring server.

Clients may omit this option for any request, even if they were referred by another server. Servers must not allow or deny access based solely on the value of this option, but they may provide different data depending on its value.

## Request Agent and Response Agent

The Request Agent and Response Agent options (codes 0x85 and 0x87, respectively) are metadata options that allow clients and servers to identify themselves. Both must have string values, and these values should in some way identify the client or server application, but not the user of that application.

Servers must not allow or deny access based solely on the value of this option, but they may provde different data or resources depending on its value.

## Compression

The Compression option (code 0x8c) is a non-mandatory option with differing semantics for clients and servers.

A server may set this option to indicate that the response's payload is compressed. If it is set, then the payload must be a byte string containing the compressed data. If the payload is any other type, the packet is malformed.

If the server sends a compressed payload and sets the Response Type option, the response type or types specified apply to the uncompressed data.

A client may set the Compression option to an array indicating the compression algorithms it supports. Servers should take this option into account before sending replies. If a server cannot fulfill the requirements of the client's Compression option, it must instead send an uncompressed payload in its response.

For servers, the Compression option may be either a string indicating the compression algorithm in use (such as `gzip`) or an integer code, as detailed below. For clients, the option is instead an array of these strings or integer codes.

### Standard compression algorithms

A server must support at least the `gzip` compression algorithm. Popular algorithms are also assigned simple integer codes for a more compact representation. These are shown in the following table, along with their string values.

| Algorithm      | String Name | Code |
|----------------|-------------|------|
| No compression | `none`      | 0x00 |
| Gzip           | `gzip`      | 0x01 |
| DEFLATE        | `deflate`   | 0x02 |
| Brotli         | `brotli`    | 0x03 |

"No compression" is considered the default, and is only listed for completeness. Servers should assume this value is present in any client request containing a Compression option.

## Valid Until

The Valid Until option (code 0x8e) is a non-mandatory option. Servers may set this option to indicate to a client that the response will expire at a specific point in the future.

This option's value must be one of: a string in the format described by RFC 4287; a CBOR-tagged date string in the RFC 4287 format (CBOR tag type 0); or a CBOR-tagged unsigned integer number of seconds since the Unix epoch of 1970-01-01T00:00Z UTC (CBOR tag type 1).

## Path

The Path option (code 0x8f) is a non-mandatory option.

If the server to which a client has made a request must pass part or all of that request one or more other servers, it should set the Path option to an array indicating all of the "hops" it has taken.

This option is generally intended for proxy servers, gateways, and other "indirect" services.

# New error codes

## Unsupported Action

The Unsupported Action error (code 0x60) is used when the server cannot process the request because it does not support the requested action for the requested resource.

Servers must include within the Additional Information section of the Error Response an array of 1-byte integers corresponding to the request actions the resource supports.

Servers may allow different sets of supported actions for different instances of a resource. If this is the case, an Unsupported Action error must also include the Instance Identifier that the client used in the request.

## Moved and Moved Temporarily

The Moved and Moved Temporarily errors (codes 0x62 and 0x63, respectively) indicate to a client that the resource requested is no longer available at a given location. This may be because it was moved by a Move action, or the server has changed its routing of locations to resources, or any other sensible reason.

Moved Temporarily is, as its name suggests, a temporary condition. Move implies that the new location is the permanent home of the requested resource. Clients should not make repeated requests to the original address upon receiving a Moved error.

Servers must include as Additional Information a string containing the new location of the resource in the Wenc address format described above.

## Censored Content

The Censored Content error (code 0x66) indicates that a resource is not available due to legal or social reasons, rather than technical issues.

Servers must include as Additional Information an array of two text strings. The first string must be a plain text reason for the removal of the resource. The second string must be a URI allowing a client user to dispute the removal.

The removal of one instance of a resource does not imply that the resource as a whole has been removed.

## Unsupported Type

The Unsupported Type error (code 0x68) informs the client that the server cannot handle a request payload of a given type.

Servers should include as Additional Information an array of text strings indicating the allowed types for the request.

If the client used an Instance Identifier to request an instance of a resource, the Unsupported Type error does not imply that all instances of that resource cannot accept payloads of the given type.

This error code does not imply that the contents of the payload are valid or invalid.

## Locked

The Locked error (code 0x69) is issued when a client attempts to write to a resource that is currently locked using the Lock or Exclusive Lock actions, or attempts to read from a resource currently locked by the Exclusive Lock actions.

Servers may use the Valid Until option to indicate the expiration time of the lock, as an informational hint to the client.

## Timeout

The Timeout error (code 0x6d) indicates that the server required additional data to process a client's request, but this data was not sent in a timely fashion.

The semantics of this error code are application-defined. Servers should use the Valid Until option on Response packets that require further input from the client, as an indication of the time until timeout.

## Too Large

The Too Large error (code 0x6e) indicates that the server received a payload that was too large to handle.

Servers should include as Additional Information the maximum allowed size for a request payload.

# Clef interoperation

## Opcodes

### Request and Response

The Request and Response opcodes are the primary means by which Wenc clients and servers communicate. Clients send Request packets to the server, indicating the resource(s) they require, and servers answer with Response packets.

Rather than a separate opcode for each action, the request action is encoded as a Request Action packet option. This option has code 0x80, and is a 1-byte integer, with the possible values described above.

Response packets continue to have application-specific payloads.

### Discovery Request and Discovery Response

Wenc clients may send Discovery Request packets to the server, and the server should respond with proper Discovery Response packets. In the context of Wenc, Discovery Request and Discovery Response should serve as ways to communicate documentation about a resource or API, similar to REST documentation tools such as Swagger.

### TLS Negotiation and TLS Response

The Clef specification defines a method for opportunistic TLS using the TLS Negotiation and TLS Response opcodes. Wenc does not add any additional semantics to these opcodes. If an implementation wishes to support opportunistic TLS, it must maintain the compatibility requirements of Clef, and it must treat self-signed certificates as first-class citizens, using a method such as Trust On First Use. Clients must not reject TLS Responses solely because they are signed with a self-signed certificate.

### Authenticate and Authentication Response

Authentication in Wenc is handled in the same way as in the base Clef specification. Clients send an Authenticate packet containing credentials, and servers respond with an Authentication Response. This response will typically have a token that can be used to authorize the client for future operations. Wenc does not define the format of this token, though servers should support at least JSON Web Tokens (JWT) and CBOR Web Tokens at a minimum.

### Batch

Wenc maintains the semantics of the Batch opcode. Clients may use multiple request actions within a batch, and servers must handle these actions in the order in which they appear in the batch. This means, for example, that a Get action on a resource that has already been the subject of an Update earlier in the batch must return the updated state of the resource.

Servers may use transactional locking within a batch, treating the entire batch as an atomic unit, so that concurrent users do not see intermediate results for any resources created, updated, or deleted within the batch.

### Error Response

Wenc defines new standard error codes, as detailed above. Beyond that, the semantics of the Error Response opcode are unchanged. Unlike HTTP, there is no universal status code, nor are there error "classes" such as the 4xx series. The fact that a server sent an Error Response instead of a Response or other packet is enough to indicate that the operation failed; clients can then inspect the packet, following the Clef specification, for any additional information they require.

Redirection is technically considered an error using the Moved or Moved Temporarily error codes.

### Continue

Wenc allows the Continue opcode, but does not further define its semantics. As in the base Clef specification, only clients are permitted to send the opcode, while servers may set a Continuation Marker option on any response to allow for specifying which action should be continued. If this option is not present, servers may assume that the client wishes to continue the most recently performed action.

### Acknowledge

Wenc allows a server to use the Clef Acknowledge opcode instead of a Response if and only if the response payload would be empty.

### Disconnect

Nost Wenc data transmissions are simple Request/Response pairs, not long-lived connections. Clients and server may, however, choose to use a persistent two-way (WebSocket-like) connection instead. Disconnect is only necessary if such a connection is used.

## Protocol options

Most Clef protocol options are used without any changes to semantics. Those which have Wenc-specific semantics or inconsistencies with the base Clef spec are described below.

### Session Identifier

The Session Identifier option (code 0x03) is only necessary for sessions that extend beyond the life of a single connection.

### Authorization

For the Authorization option (code 0x05), the preferred format of the option is a CBOR Web Token.

### Ping

The Ping option (code 0x0e) is not quite functionally equivalent to the Options request action. Wenc servers should return an informative help message in response to a request with the Ping option enabled.

### Continuation Marker

Wenc additionally allows the client to set the Continuation Marker option to indicate that it is sending an incomplete payload. This can be, for example, because the server only permits a certain payload size.

In addition, servers may set the Continuation Marker to indicate an incomplete payload, such as for a partial file download.

## Standard options

Most standardized options in Clef are used in Wenc without change to semantics. Those which are different are listed below.

### Range

The Range option (code 0x1e) has slightly different semantics when used with the List action, as described in that section.

### Target

The Target option (code 0x3e) is defined for the Copy and Move request actions as the destination resource. It remains application-defined for other actions.

## Standard error codes

Wenc servers can return all of the standard Clef error codes. In those cases where there are differing or more explicit semantics, they are described below.

### Unauthorized and Incorrect Credentials

The Unauthorized and Incorrect Credentials errors (codes 0x38 and 0x39, respectively) are similar, but have specific use cases.

Servers must return an Error Response with Unauthorized when a client attempts to access a resource that requires authorization, and the client does not provide any authorization credentials Servers must instead return an Error Response with Incorrect Credentials when the client does provide credentials, but these are not correct.

# Comparison to HTTP

This is a brief overview of the main differences between Wenc and HTTP.

* Wenc is a strongly-typed binary protocol. Unlike HTTP, where metadata is always plain text, Wenc's header and options do require interpretation. In exchange, servers and clients both get better error-checking and validation, as well as a small but noticeable size benefit.

* Wenc has different classes of requests and responses. These are the "opcodes" of Clef, and they represent the broad purpose of a packet, such as a Request, Response, or Error Response. In some ways, these are similar to HTTP's status code categories, but client-sent packets also have them.

* Service discovery is baked into the Wenc protocol. Clef offers the Ping option, which is intended to provide a standardized way of accessing a "help" response, while the Discovery Query and Discovery Response are a builtin solution to API documentation.

* Wenc prefers opportunistic TLS. There is no need for a separate `wencs` scheme to indicate that a server connection is secure. Instead, dedicated packets begin a secure session.

* Wenc has stronger session and state support. The Session Identifier option allows for a standardized counterpart to the session ID cookie. In addition, because Wenc allows and encourages persistent connections, many transactional requests can be perfomed in series with less need for such tracking.

* Instead of HTTP's overloaded GET method, Wenc has dedicated actions for retrieving a single resource and a list of them. The standdard Range option also allows cursor-style or paginated access to item lists without the need for query parameters.

* In fact, Wenc has no query parameters, or any need for them. All requests can contain a body of strongly-typed data, and clients may send a data map cotaining what an HTTP GET would require as query parameters.

* This strong type system also means that Wenc natively supports multipart packets with ease. A request or response can contain an array of individual data structures, each with their own specific MIME type, so there is no need for a cumbersome "form-encoded" type.

* The Batch opcode allows multiple requests to be sent in a single packet, which can be used for better transactional support.

* Wenc has builtin support for WebDAV-style file locking, including shared and exclusive locks, as well as a standardized way of timing out resource locks to prevent deadlock conditions.

* Because of the need for part of the binary header to be a fixed size, Wenc packets are limited to a maximum length of 2 ** 32 + 16 bytes, while HTTP has no such inherent limitation.

* While Wenc can be used to transmit text data, it has no real concept of hypertext relations. It is more geared toward APIs than web pages.
