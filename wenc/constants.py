# Constants and enums for Clef and Wenc

from enum import IntEnum

# The magic number for a Clef packet. This is found at the start of the header, and it serves two purposes.
# First, it identifies the data structure as a Clef packet. Second, it combines with the opcode to create
# a custom CBOR tag for each packet type.
CLEF_MAGIC = 0xda704177

# The default port reserved for a Clef server.
METASERVER_PORT = 7476

# The protocol marker, used in a Clef header.
PROTOCOL_MARKER = 0x84

class PacketOpcode(IntEnum):
    """The byte representing the opcode of a Clef packet.
    
    This is always in the range 0x60-0x6f. Only the last 4 bits are used, but the 0x60 offset
    is necessary to preserve the CBOR tag number for the opcode.
    """

    REQUEST = 0x60
    RESPONSE = 0x61
    DISCOVERY_QUERY = 0x62
    DISCOVERY_RESPONSE = 0x63
    TLS_NEGOTIATION = 0x64
    TLS_RESPONSE = 0x65
    AUTHENTICATE = 0x66
    AUTHENTICATE_RESPONSE = 0x67
    BATCH = 0x68
    ERROR_RESPONSE = 0x69
    CONTINUE = 0x6c
    ACKNOWLEDGE = 0X6e
    DISCONNECT = 0x6f

    @classmethod
    def from_tag(cls, tag: int):
        return cls(tag & 0xff)

    def to_tag(self) -> int:
        return (CLEF_MAGIC << 8) + self.value


class ProtocolOption(IntEnum):
    """The protocol-level options for Clef and Wenc. All others are currently reserved by the specification."""

    PROTOCOL_VERSION = 0x01
    SESSION_IDENTIFIER = 0x03
    AUTHORIZATION = 0x05
    TIMESTAMP = 0x08
    EXPIRES = 0x09
    SIGNATURE = 0x0d
    SIGNING_ALGORITHM = 0x0e
    PING = 0x10
    CONTINUATION_MARKER = 0x17


class StandardOption(IntEnum):
    """Standard options for Clef packets. These are common to all packet types, and are defined by the spec itself."""

    LAST_MODIFIED = 0x19
    RANGE = 0x1e
    SERVICE_NAME = 0x20
    SERVICE_VERSION = 0x21
    TARGET = 0x3e
    INSTANCE_IDENTIFIER = 0x3f
    SERVICE_DISCOVERY = 0x40
    SERVICE_DESCRIPTION = 0x41
    VERSION_DISCOVER = 0x42
    USERNAME = 0x50
    PASSWORD = 0x51
    AUTHENTICATION_TOKEN = 0x52

    # Wenc-specific additions

    REQUEST_ACTION = 0x80
    REQUEST_TYPE = 0x81
    RESPONSE_TYPE = 0x83
    REFERRER = 0x84
    REQUEST_AGENT = 0x85
    COMPRESSION = 0x8c
    VALID_UNTIL = 0x8e
    PATH = 0x8f


class DiscoveryTypeCode(IntEnum):
    """Standardized type codes for discovery responses."""

    UINT8 = 0x00
    UINT16 = 0x01
    UINT32 = 0x02
    UINT64 = 0x63
    INT8 = 0x04
    INT16 = 0x05
    INT32 = 0x06
    INT64 = 0x07
    FLOAT16 = 0x80
    FLOAT32 = 0x09
    FLOAT64 = 0x0a
    BOOLEAN = 0x0c
    STRING = 0x10
    POINTER = 0x11
    ARRAY = 0x12
    MAP = 0x13
    BINARY_DATA = 0x14
    DATETIME = 0x15
    TIMESTAMP = 0x16


class ErrorCode(IntEnum):
    """Standardized error response codes. This does not include the "private use" codes in the range 0x40-0x5a."""

    VALIDATION_FAILURE = 0x22
    PAYMENT_REQUIRED = 0x24
    HASH_FAILURE = 0x25
    INSTANCE_UNAVAILABLE = 0x2a
    EXPIRED = 0x2e
    BAD_REQUEST = 0x2f
    REFUSED = 0x30
    VERSION_UNAVAILABLE = 0x34
    NO_SERVICE_MATCH = 0x37
    UNAUTHORIZED = 0x38
    INCORRECT_CREDENTIALS = 0x39
    FORBIDDEN = 0x3a
    AUTHORIZATION_REQUIRED = 0x3b
    CONFLICT = 0x3d
    NOT_IMPLEMENTED = 0x3f
    ENCRYPTION_REQUIRED = 0x5f

    # Wenc uses error codes 0x60-0x6f

    UNSUPPORTED_ACTION = 0x60
    MOVED = 0x62
    MOVED_TEMPORARILY = 0x63
    CENSORED_CONTENT = 0x66
    UNSUPPORTED_TYPE = 0x68
    LOCKED = 0x69
    TIMEOUT = 0x6d
    TOO_LARGE = 0x6e

    # End Wenc error codes

    SERVER_ERROR = 0x70
    RATE_LIMIT_EXCEEDED = 0x72
    INSUFFICIENT_RESOURCES = 0x73
    UNSUPPORTED_PROTOCOL = 0x7c
    INVALID_OPERATION = 0x7f


class ActionType(IntEnum):
    """Wen request action types."""

    GET = 0x80
    LIST = 0x81
    CREATE = 0x82
    UPDATE = 0x83
    DELETE = 0x84
    COPY = 0x85
    MOVE = 0x86
    OPTIONS = 0x88
    LOCK = 0x8c
    UNLOCK = 0x8d
    EXCLUSIVE_LOCK = 0x8e


class CompressionAlgorithm(IntEnum):
    """Wenc standardized compression algorithm codes."""

    NONE = 0x00
    GZIP = 0x01
    DEFLATE = 0x02
    BROTLI = 0x03
