# Clef/Wenc packet decoder and supporting functions.

from io import BytesIO
from hashlib import blake2s
from typing import Optional, Union
import cbor2

from .constants import CLEF_MAGIC, PacketOpcode, ProtocolOption, StandardOption
from .exceptions import IncompletePacketError, InvalidEncodedValueError, InvalidHashError, InvalidMagicNumberError, InvalidOpcodeError, PacketDecodeError, ReservedOptionDecodeError
from .types import AcknowledgePacket, AuthenticatePacket, AuthenticationResponsePacket, BatchPacket, ClefPacket, ContinuePacket, DisconnectPacket, DiscoveryQueryPacket, DiscoveryResponseEntry, DiscoveryResponsePacket, DiscoveryResponseParameter, DiscoveryResponsePayloadMultiple, DiscoveryResponsePayloadSingle, ErrorPayload, ErrorResponsePacket, RawPacket, RequestPacket, ResponsePacket, TlsNegotiationPacket, TlsResponsePacket

class ClefDecoder:
    """A decoder for Clef and Wenc data packets.
    
    This class has two main functions for decoding:
    * decode - Deserializes a packet into a custom object
    * decode_raw - Deserializes a packet into a RawPacket object,
        without decoding for opcode-specific fields. This structure is the same
        as if you decoded the packet manually.

    All types handled by the underlying cbor2 library are decoded as in
    that library, including tagged types for datetime objects, long integers,
    decimal types, rational numbers, regular expressions, UUIDs, and IP addresses.

    At present, custom types are deserialized as dictionaries, because it isn't
    possible to store the semantic structure of an object in a CBOR data stream.
    In the future, I'd like to add support for this using custom tags or something.
    """

    _decoder : Optional[cbor2.CBORDecoder]

    def __init__(self) -> None:
        """Create a new decoder instance."""
        self._decoder = None

    def _decode(self, packet: BytesIO) -> RawPacket:
        MIN_PACKET_SIZE = 18    # 16 header bytes + 1 empty options byte + 1 empty payload byte
        with packet:
            packet_header = bytearray(MIN_PACKET_SIZE)

            header_read_size = packet.readinto(packet_header)
            if header_read_size < MIN_PACKET_SIZE:
                # A packet must be no less than 18 bytes in length: 16 for the header, 1 each for options and payload.
                raise IncompletePacketError(f"Packet length must be at least 18 bytes: got {header_read_size}")

            magic = int.from_bytes(packet_header[:4], "big")
            if magic != CLEF_MAGIC:
                # Magic number is incorrect, so this isn't actually a Clef packet.
                raise InvalidMagicNumberError(f"Magic number is {hex(magic)} instead of {hex(CLEF_MAGIC)}")

            try:
                # Check that the opcode byte is in the correct range, and isn't a reserved value.
                opcode_byte = packet_header[4]
                if opcode_byte < 0x60 or opcode_byte > 0x6f:
                    raise InvalidOpcodeError(f"{hex(opcode_byte)} is not a valid opcode")
                    
                opcode = PacketOpcode(opcode_byte)
            except ValueError as exc:
                raise InvalidOpcodeError(f"Opcode {hex(opcode_byte)} is reserved")

            try:
                # Now try decoding the message as a CBOR object.

                # We used the stream earlier, so run it back to the beginning.
                packet.seek(0)
                decoded_message = cbor2.load(packet)

                # Sanity check that the opcode is the same as the lowest byte of the tag.
                assert opcode == (decoded_message.tag & 0xff)

                length, packet_hash, options, payload = decoded_message.value

                # Verify the content length is correct.
                packet_size = packet.tell()
                if packet_size != length + MIN_PACKET_SIZE - 2:
                    raise IncompletePacketError(f"Invalid content length: expected {length}, got {packet_size}")

                # Verify that the hash is correct.
                packet.seek(MIN_PACKET_SIZE - 2)
                calculated_hash = blake2s(digest_size=4)
                calculated_hash.update(packet)
                if calculated_hash.digest() != packet_hash:
                    raise InvalidHashError(f"Expected {packet_hash.hex()}, got {calculated_hash.hexdigest()}")
            except cbor2.CBORDecodeValueError as exc:
                raise InvalidEncodedValueError(str(exc)) from exc
            except cbor2.CBORDecodeEOF as exc:
                raise IncompletePacketError("EOF encountered")
            except cbor2.CBORDecodeError as exc:
                raise PacketDecodeError(str(exc)) from exc
            except Exception as exc:
                raise PacketDecodeError(str(exc)) from exc

        return RawPacket(opcode, length, packet_hash, options, payload)

    def decode_raw(self, packet: Union[bytes, BytesIO]) -> RawPacket:
        """Decode a Clef packet as a raw packet, ignoring the semantic content of the packet."""

        return self._decode(packet if isinstance(packet, BytesIO) else BytesIO(packet))

    def decode(self, packet: Union[bytes, BytesIO], *,
        ignore_bad_options: bool = False
    ) -> ClefPacket:
        """Decode a Clef packet. The packet can be either a file-like object that is open for reading
        in binary mode, or a bytes object."""

        raw_packet = self._decode(packet if isinstance(packet, BytesIO) else BytesIO(packet))

        # For this one, we need to convert the raw packet into a structured one based on the opcode.

        opcode = raw_packet.opcode

        # Separate the options into the three different categories.
        # We're going to merge them again, but this lets us do some better error checking.
        # TODO: Actually write that error checking.
        protocol_options = {}
        standard_options = {}
        application_options = {}
        for k,v in raw_packet.options.items():
            if k in ProtocolOption:
                protocol_options[k] = v
            elif k in StandardOption:
                standard_options[k] = v
            elif isinstance(k, str):
                application_options[k] = v
            elif not ignore_bad_options:
                # This packet uses an integer option that isn't defined.
                # It's an error unless the user didn't specifically requests to ignore it.
                raise ReservedOptionDecodeError(f"Encountered reserved option {hex(k)}")

        common_parameters = (protocol_options + standard_options + application_options)

        # Basically a switch on all known opcode types, so we get the correct class.
        if opcode == PacketOpcode.REQUEST:
            result = RequestPacket(*common_parameters, raw_packet.payload)
        elif opcode == PacketOpcode.RESPONSE:
            result = ResponsePacket(*common_parameters, raw_packet.payload)
        elif opcode == PacketOpcode.DISCOVERY_QUERY:
            result = DiscoveryQueryPacket(*common_parameters)
        elif opcode == PacketOpcode.DISCOVERY_RESPONSE:
            if not isinstance(raw_packet.payload, dict):
                # This is a single discovery response.
                try:
                    entry = DiscoveryResponsePayloadSingle(
                        name=raw_packet.payload.get("name"),
                        description=raw_packet.payload.get("description"),
                        parameters=[DiscoveryResponseParameter(
                            name=p.get("name"),
                            type=p.get("type"),
                            required=p.get("req", False),
                            default=p.get("def", None)
                        ) for p in raw_packet.payload.get("parameters", [])],
                        authenticated=raw_packet.payload.get("auth", False),
                        application_fields={k: v for k,v in raw_packet.payload if k not in ("name", "description", "parameters", "auth")}
                    )
                    result = DiscoveryResponsePacket(*common_parameters, entry)
                except Exception as exc:
                    raise InvalidEncodedValueError(str(exc)) from exc
            else:
                # This is a list of discovery responses.
                entries = {}
                for resource_name, resource in raw_packet.payload.items():
                    try:
                        entries[resource_name] = DiscoveryResponseEntry(
                            name=resource.get("name"),
                            description=resource.get("description"),
                            parameters=[DiscoveryResponseParameter(
                                name=p.get("name"),
                                type=p.get("type"),
                                required=p.get("req", False),
                                default=p.get("def", None)
                            ) for p in resource.get("parameters", [])],
                            authenticated=resource.get("auth", False),
                            application_fields={k: v for k,v in resource if k not in ("name", "description", "parameters", "auth")}
                        )
                    except Exception as exc:
                        raise InvalidEncodedValueError(f"Could not decode resource {resource_name}: {exc}") from exc
                result = DiscoveryResponsePacket(*common_parameters, DiscoveryResponsePayloadMultiple(entries))
        elif opcode == PacketOpcode.TLS_NEGOTIATION:
            result = TlsNegotiationPacket(*common_parameters)
        elif opcode == PacketOpcode.TLS_RESPONSE:
            result = TlsResponsePacket(*common_parameters)
        elif opcode == PacketOpcode.AUTHENTICATE:
            result = AuthenticatePacket(*common_parameters)
        elif opcode == PacketOpcode.AUTHENTICATE_RESPONSE:
            result = AuthenticationResponsePacket(*common_parameters, raw_packet.payload)
        elif opcode == PacketOpcode.BATCH:
            # TODO: More error checking, fix types.
            result = BatchPacket(*common_parameters, [p.value for p in raw_packet.payload])
        elif opcode == PacketOpcode.ERROR_RESPONSE:
            try:
                code, description, additional = raw_packet.payload
                result = ErrorResponsePacket(*common_parameters, ErrorPayload(code, description, additional))
            except ValueError as exc:
                raise InvalidEncodedValueError("Error response is malformed")
        elif opcode == PacketOpcode.CONTINUE:
            result = ContinuePacket(*common_parameters, raw_packet.payload)
        elif opcode == PacketOpcode.ACKNOWLEDGE:
            result = AcknowledgePacket(*common_parameters)
        elif opcode == PacketOpcode.DISCONNECT:
            result = DisconnectPacket(*common_parameters, raw_packet.payload)
        else:
            # Shouldn't happen because of the sanity checks earlier.
            raise InvalidOpcodeError(f"Reserved opcode {hex(opcode)}")

        return result
