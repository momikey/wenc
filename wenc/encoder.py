# Clef/Wenc packet encoder and supporting functions.

from io import BytesIO
from typing import Any, Dict, Optional, Union
from hashlib import blake2s
import cbor2
from .constants import CLEF_MAGIC, PROTOCOL_MARKER, PacketOpcode
from .exceptions import InvalidValueError, PacketEncodeError, TypeSerializationError

from .types import ClefPacket, RawPacket

class WencEncoder:
    """An encoder for Clef/Wenc packets.
    
    This class has two main functions for encoding data:
    * encode - Serializes a ClefPacket (or subclass) object into a data string.
    * encode_raw - Serializes a RawPacket object or a series of parameters:
        opcode, continuation marker, options, and payload.

    All types that the underlying CBOR2 library can serialize are encoded as in
    that library. This includes datetime, Decimal, long integers, rational numbers,
    regular expression objects, and UUID objects.

    At present, custom types are not supported. This will change in the future.
    """

    # So that we can customize the way the encoder works, if necessary.
    _encoder : Optional[cbor2.CBOREncoder]

    def __init__(self) -> None:
        """Create a new encoder instance."""
        self._encoder = None

    def _encode(self, packet: RawPacket) -> bytes:
        # We have to do some work to handle the fixed-width values for content length and hash.
        # This requires us to build our own CBOR integers. A 4-byte unsigned integer is always prefixed
        # with the byte 0x1a (0b0001_1010) for major type 0, additional information 26).
        PREFIX_BYTE = b'\x1a'

        if packet.opcode is None:
            raise PacketEncodeError("An opcode must be provided")

        try:
            # CBOR-encode the options and payload sections.
            options_section = cbor2.dumps(packet.options if packet.options is not None else {})
            payload_section = cbor2.dumps(packet.payload)

            # Calculate the length of the packet's content. This is options + payload, not counting
            # the fixed 16 bytes of the header.
            content_length = len(options_section) + len(payload_section)

            # Calculate the 4-byte BLAKE2s hash of the options and payload sections as a whole.
            # This is the packet checksum; it isn't intended to be anything cryptographically secure.
            # Note: The RawPacket class defines a hash attribute, but that's for storing.
            packet_hash = blake2s(options_section + payload_section, digest_size=4).digest()

            # Create the header. We can't do this using the CBOR library itself, but it's a pretty simple
            # header to construct manually.
            header = b"".join((
                CLEF_MAGIC.to_bytes(4, "big"),
                packet.opcode.to_bytes(1, "big"),
                PROTOCOL_MARKER.to_bytes(1, "big"),
                PREFIX_BYTE,
                content_length.to_bytes(4, "big"),
                PREFIX_BYTE,
                packet_hash
            ))

            message = b"".join((header, options_section, payload_section))
        except cbor2.CBOREncodeTypeError as exc:
            raise TypeSerializationError(str(exc)) from exc
        except cbor2.CBOREncodeValueError as exc:
            raise InvalidValueError(str(exc)) from exc
        except cbor2.CBOREncodeError as exc:
            raise PacketEncodeError(str(exc)) from exc
        except Exception as exc:
            raise PacketEncodeError(str(exc)) from exc

        return message

    def encode_raw(self, *,
        packet: Optional[RawPacket] = None,
        opcode: Optional[PacketOpcode] = None,
        options: Dict[Union[int,str], Any] = None,
        payload: Any = None
    ) -> bytes:
        """Encode raw data into a byte string.
        
        This method can take either a pre-prepared RawPacket object or the various
        parts comprising it.

        Note: When encoding, the `length` field of the RawPacket object is ignored,
        as the length is calculated based on the data in the options and payload sections.
        """

        if packet is None:
            packet = RawPacket(opcode=opcode, options=options, payload=payload)
        return self._encode(packet)

    def encode(self, packet: ClefPacket) -> bytes:
        """Encode a packet object into a byte string.
        
        This method operates on objects that are subclasses of ClefPacket. These are
        tailored to each specific opcode.
        """

        return self._encode(packet.to_raw())
