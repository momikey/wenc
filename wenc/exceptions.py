# Custom exceptions for Clef/Wenc error conditions.

class ClefError(Exception):
    """Base class for all Clef errors. This class should not be created directly."""


class PacketEncodeError(ClefError):
    """An error in encoding a Clef packet."""


class TypeSerializationError(PacketEncodeError):
    """Indicates an error while attempting to serialize a user-defined type."""


class InvalidValueError(PacketEncodeError):
    """Indicates an invalid value was encountered while serializing."""


class PacketDecodeError(ClefError):
    """An error in decoding a Clef packet."""


class IncompletePacketError(PacketDecodeError):
    """Indicates that a packet is too short, according to its content length."""


class InvalidMagicNumberError(PacketDecodeError):
    """Indicates that the magic number is incorrect for a Clef packet."""


class InvalidOpcodeError(PacketDecodeError):
    """Indicates that the packet's opcode is invalid.

    This can mean that the opcode is outside the range (0x60...0x6f),
    or that it is a reserved value.
    """


class InvalidHashError(PacketDecodeError):
    """Indicates that the packet's hash does not match the calculated hash of its contents."""


class InvalidEncodedValueError(PacketDecodeError):
    """Indicates that there was an error attempting to deserialize a value."""


class ReservedOptionDecodeError(PacketDecodeError):
    """Indicates that an invalid or reserved integer option was passed."""


class WencError(ClefError):
    """Base class for Wenc-specific exceptions. This class should not be created directly."""


class InvalidActionError(WencError):
    """Indicates that the request action code is invalid."""
