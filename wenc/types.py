# Type classes for Clef

from abc import ABC, abstractmethod
from typing import Any, Dict, List, Optional, Union

from attrs import define, field

from .constants import PacketOpcode

# Payloads for specifc packet types.

@define
class RequestPayload:
    """The payload for a Request packet. This contains a map of named parameters
    followed by an array of unnamed parameters. For Python, these are encoded as
    a dict and a list."""

    named_parameters: Dict[str, Any] = field(factory=dict)
    unnamed_parameters: List[Any] = field(factory=list)


@define
class DiscoveryResponseParameter:
    """A parameter description for a resource. The Discovery Reponse payload includes
    a list of these for the requested resource."""

    name: Optional[str] = None
    type: Optional[Union[int, str]] = None
    required: bool = False
    default: Any = None


@define
class DiscoveryResponseEntry:
    """A single entry in a Discovery Response payload. These contain a few fixed
    values plus any number of application-defined ones."""

    name: Optional[str] = None
    description: Optional[str] = None
    parameters: List[DiscoveryResponseParameter] = field(factory=list)
    authenticated: bool = False
    application_fields: Dict[str, Any] = field(factory=dict)
    version: Optional[Union[int, bytes]] = None
    latest: Optional[bool] = None


@define
class DiscoveryResponsePayloadSingle(DiscoveryResponseEntry):
    """The full payload for a Discovery Response packet with a single entry."""


@define
class DiscoveryResponsePayloadMultiple:
    """The full payload for a Discovery Response packet with multiple entries."""

    entries: Dict[str, DiscoveryResponseEntry] = field(factory=dict)


@define
class ErrorPayload:
    """The payload for an Error packet. This includes an error code and description."""

    error_code : int
    description: str
    additional_information: Dict[str, Any] = field(factory=dict)


# Classes for packet types.

@define
class RawPacket:
    """A raw Clef packet. This is not checked for any semantic information. It's just
    the length, hash, options, and payload, as described in the Clef spec."""

    opcode: int
    length: int = 0
    hash : int = 0
    options: Dict[Union[int,str], Any] = field(factory=dict)
    payload: Any = None


@define
class ClefPacket(ABC):
    """Base type for Clef packets. This defines a common interface  all packets,
     but leaves specifics to subclasses."""

    options: Dict[Union[int, str], Any] = field(factory=dict)

    @abstractmethod
    def to_raw(self) -> RawPacket:
        """Convert this packet object to a raw packet for encoding."""
        # For the base class, this method has to be abstract, but we can start building the
        # raw packet object here.
        return RawPacket(opcode=0, options=self.options)


@define
class RequestPacket(ClefPacket):
    """A Clef Request packet."""

    payload: RequestPayload = field(factory=RequestPayload)

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.REQUEST
        raw.payload = [self.payload.named_parameters, self.payload.unnamed_parameters]
        return raw


@define
class ResponsePacket(ClefPacket):
    """A Clef Response packet. The payload is application-defined."""

    payload: Any = None

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.RESPONSE
        raw.payload = self.payload
        return raw


@define
class DiscoveryQueryPacket(ClefPacket):
    """A Clef Discovery Query packet. No defined payload."""

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.DISCOVERY_QUERY
        return raw


@define
class DiscoveryResponsePacket(ClefPacket):
    """A Clef Discovery Response packet. The payload may be either a single Discovery Response object
    or a map of strings to those objects."""

    payload: Union[DiscoveryResponsePayloadSingle, DiscoveryResponsePayloadMultiple] = None

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.DISCOVERY_RESPONSE
        if isinstance(self.payload, DiscoveryResponsePayloadSingle):
            payload = [self.payload]
        else:
            payload = self.payload
        raw.payload = [response_item.update({
            "name": self.payload.name,
            "description": self.payload.description,
            "parameters": [{
                "name": p.name,
                "type": p.type,
                "req": p.required,
                "def": p.default
            } for p in self.payload.parameters],
            "auth": self.payload.authenticated,
            "version": self.payload.version,
            "latest": self.payload.latest
        }) for response_item in payload]
        return raw


@define
class TlsNegotiationPacket(ClefPacket):
    """A Clef TLS Negotiation packet. No payload is necessary."""

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.TLS_NEGOTIATION
        return raw


@define
class TlsResponsePacket(ClefPacket):
    """A Clef TLS Response packet. No payload is necessary."""

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.TLS_RESPONSE
        return raw


@define
class AuthenticatePacket(ClefPacket):
    """A Clef Authenticate packet. No payload is necessary, as the options map contains all needed data."""

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.AUTHENTICATE
        return raw


@define
class AuthenticationResponsePacket(ClefPacket):
    """A Clef Authentication Response packet. The payload is a byte string which is an authentication token."""

    token: bytes = field(default=b"")

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.AUTHENTICATE_RESPONSE
        raw.payload = self.token
        return raw


@define
class BatchPacket(ClefPacket):
    """A Clef Batch packet. The payload is a list of packets."""

    payload: List[ClefPacket] = field(factory=list)

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.BATCH
        raw.payload = [p.to_raw for p in self.payload]
        return raw


@define
class ErrorResponsePacket(ClefPacket):
    """A Clef Error Response packet. The payload is an error response payload."""

    payload: ErrorPayload = field(factory=ErrorPayload)

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.ERROR_RESPONSE
        raw.payload = [self.payload.error_code, self.payload.description, self.payload.additional_information]
        return raw


@define
class ContinuePacket(ClefPacket):
    """A Clef Continue packet. The payload is application-defined."""

    payload: Any = None

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.CONTINUE
        raw.payload = self.payload
        return raw


@define
class AcknowledgePacket(ClefPacket):
    """A Clef Acknowledge packet. No payload is defined."""

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.ACKNOWLEDGE
        return raw


@define
class DisconnectPacket(ClefPacket):
    """A Clef Disconnect packet. The payload is application-defined."""

    payload: Any = None

    def to_raw(self) -> RawPacket:
        raw = super().to_raw()
        raw.opcode = PacketOpcode.DISCONNECT
        raw.payload = self.payload
        return raw
